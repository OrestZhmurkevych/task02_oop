package edu.aviacompany.domain;

public abstract class Plane {
    private String id;
    private String name;
    private int tankCapacity;
    private int oilConsumption;
    private PlaneType planeType;

    public Plane() {
    }

    public Plane(final String id, final String name, final int tankCapacity, final int oilConsumption) {
        this.id = id;
        this.name = name;
        this.tankCapacity = tankCapacity;
        this.oilConsumption = oilConsumption;
        this.planeType = PlaneType.NONE;
    }

    @Override
    public String toString() {
        return "\nPlane{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", tankCapacity=" + tankCapacity +
                ", oilConsumption=" + oilConsumption +
                ", planeType=" + planeType +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(int tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public int getOilConsumption() {
        return oilConsumption;
    }

    public void setOilConsumption(int oilConsumption) {
        this.oilConsumption = oilConsumption;
    }

    public PlaneType getPlaneType() {
        return planeType;
    }

    public void setPlaneType(PlaneType planeType) {
        this.planeType = planeType;
    }
}
