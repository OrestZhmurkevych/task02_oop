package edu.aviacompany.domain;

public class CargoPlane extends Plane{
    private int carryingCapacity;

    public CargoPlane(final String id, final String name, final int tankCapacity,
                      final int carryingCapacity, final int oilConsumption) {
        super(id, name, tankCapacity, oilConsumption);
        this.carryingCapacity = carryingCapacity;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

}
