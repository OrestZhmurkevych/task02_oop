package edu.aviacompany.domain;

public class PassengerPlane extends Plane {
    private int seatsCapacity;

    public PassengerPlane() {
    }

    public PassengerPlane(final String id, final String name, final int tankCapacity,
                          final int oilConsumption, final int seatsCapacity) {
        super(id, name, tankCapacity, oilConsumption);
        this.seatsCapacity = seatsCapacity;
    }

    public int getSeatsCapacity() {
        return seatsCapacity;
    }

    public void setSeatsCapacity(int seatsCapacity) {
        this.seatsCapacity = seatsCapacity;
    }
}
