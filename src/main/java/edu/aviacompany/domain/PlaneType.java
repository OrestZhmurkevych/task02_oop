package edu.aviacompany.domain;

public enum PlaneType {
    NONE,
    CARGO,
    PASSENGER,
    SPECIAL;
}
