package edu.aviacompany.domain;

public class SpecialPlane extends Plane {
    private int seatsCapacity;
    private int carryingCapacity;

    public SpecialPlane(final String id, final String name, final int tankCapacity,
                        final int seatsCapacity, final int carryingCapacity, final int oilConsumption) {
        super(id, name, tankCapacity, oilConsumption);
        this.seatsCapacity = seatsCapacity;
        this.carryingCapacity = carryingCapacity;
    }

    public int getSeatsCapacity() {
        return seatsCapacity;
    }

    public void setSeatsCapacity(int seatsCapacity) {
        this.seatsCapacity = seatsCapacity;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }
}
