package edu.aviacompany.service;


import edu.aviacompany.domain.*;

import java.util.ArrayList;
import java.util.List;

public class Initializer {

    public List<Plane> initData() {
        Plane boeing747 = new PassengerPlane("01P", "Boeing 747",
                183000, 12, 520);
        boeing747.setPlaneType(PlaneType.PASSENGER);
        Plane boeing737 = new PassengerPlane("02P", "Boeing 737",
                30000, 3, 143);
        boeing737.setPlaneType(PlaneType.PASSENGER);
        Plane boeing777 = new PassengerPlane("03P", "Boeing 777",
                195285, 15, 396);
        boeing777.setPlaneType(PlaneType.PASSENGER);
        Plane airbusA380 = new PassengerPlane("04P", "Airbus A380",
                320000, 14, 853);
        airbusA380.setPlaneType(PlaneType.PASSENGER);
        Plane mcDonnelDouglas = new PassengerPlane("05P", "McDonnelDouglas DC-10",
                138250, 8, 380);
        mcDonnelDouglas.setPlaneType(PlaneType.PASSENGER);
        Plane boeingCargo = new CargoPlane("01C", "Boeing Cargo",
                190000, 150, 15);
        boeingCargo.setPlaneType(PlaneType.CARGO);
        Plane AN225 = new SpecialPlane("01S", "AN 225 Mriya",
                250000, 10, 200, 25);
        AN225.setPlaneType(PlaneType.SPECIAL);
        List<Plane> planeList = new ArrayList<>();
        planeList.add(boeing747);
        planeList.add(boeing737);
        planeList.add(boeing777);
        planeList.add(airbusA380);
        planeList.add(mcDonnelDouglas);
        planeList.add(boeingCargo);
        planeList.add(AN225);
        return planeList;
    }
}