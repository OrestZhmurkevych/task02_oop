package edu.aviacompany.service;

import edu.aviacompany.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Manager {

    public int getOverallSeatsCapacity(List<Plane> planeList){
        int result=0;
        for (Plane plane: planeList) {
            if(plane.getPlaneType() == PlaneType.PASSENGER){
                result += ((PassengerPlane) plane).getSeatsCapacity();
            }
            if(plane.getPlaneType() == PlaneType.SPECIAL){
                result += ((SpecialPlane) plane).getSeatsCapacity();
            }
        }
        return result;
    }

    public int getOverallCargoCapacity(List<Plane> planeList) {
        int result=0;
        for (Plane plane: planeList) {
            if(plane.getPlaneType() == PlaneType.CARGO){
                result += ((CargoPlane) plane).getCarryingCapacity();
            }
            if(plane.getPlaneType() == PlaneType.SPECIAL){
                result += ((SpecialPlane) plane).getCarryingCapacity();
            }
        }
        return result;
    }

    public Set<Plane> sortByFlightRange(List<Plane> planeList){
        FlightRangeComparator comparator = new FlightRangeComparator();
        Set<Plane> sortedPlanes = new TreeSet<>(comparator);
        sortedPlanes.addAll(planeList);
        return sortedPlanes;
    }

    public List<Plane> searchForPlaneWithSuitableOilConsumption(List<Plane> planeList, int rangeStart, int rangeEnd){
        List<Plane> planeListWithSuitableOilConsumption = new ArrayList<>();
        for (Plane plane: planeList) {
            if(plane.getOilConsumption() > rangeStart && plane.getOilConsumption() < rangeEnd) {
                planeListWithSuitableOilConsumption.add(plane);
            }
        }
        if(planeListWithSuitableOilConsumption.isEmpty() == true) {
            System.out.println("The planes with such properties are not available");
        }
        return planeListWithSuitableOilConsumption;
    }
}
