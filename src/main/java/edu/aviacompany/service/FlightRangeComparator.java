package edu.aviacompany.service;

import edu.aviacompany.domain.Plane;

import java.util.Comparator;

public class FlightRangeComparator implements Comparator {
    @Override
    public int compare(Object obj1, Object obj2) {
        final Plane plane1 = (Plane) obj1;
        final Plane plane2 = (Plane) obj2;
        final int range1 = plane1.getTankCapacity() / plane1.getOilConsumption();
        final int range2 = plane2.getTankCapacity() / plane2.getOilConsumption();
        if (range1 > range2) {
            return 1;
        } else if (range1 < range2) {
            return -1;
        } else {
            return 0;
        }
    }
}
