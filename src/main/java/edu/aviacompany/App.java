package edu.aviacompany;


import edu.aviacompany.domain.Plane;
import edu.aviacompany.service.Initializer;
import edu.aviacompany.service.Manager;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class App {

    public static void main(String[] args) {
        Initializer init = new Initializer();
        final List<Plane> planes = init.initData();
        Scanner scanner = new Scanner(System.in);
        System.out.println("The airline has such planes: \n" + planes + " ");
        Manager manager = new Manager();
        int overallSeatsCapacity = manager.getOverallSeatsCapacity(planes);
        int overallCarryingCapacity = manager.getOverallCargoCapacity(planes);
        System.out.println("The overall seats capacity of airline is: " + overallSeatsCapacity);
        System.out.println("The overall cargo capacity of airline is: " + overallCarryingCapacity);
        final Set<Plane> sortedPlanesByFlightRange = manager.sortByFlightRange(planes);
        System.out.println("\nSorted planes: \n" + sortedPlanesByFlightRange);
        System.out.print("\nEnter the starting indicator of range for search on fuel consumption --> ");
        int startRange = scanner.nextInt();
        System.out.print("Enter the ending indicator of range for search on fuel consumption --> ");
        int endRange = scanner.nextInt();
        final List<Plane> planesSuitableOnFuelConsumption = manager.searchForPlaneWithSuitableOilConsumption
                (planes, startRange, endRange);
        System.out.println("\nPlanes, suitable on fuel consumption: \n" + planesSuitableOnFuelConsumption + " ");
    }
}
